FROM alpine:3.8

RUN apk --no-cache add shadow

RUN useradd -m webapp -d /src

WORKDIR /src

ADD https://s3-us-west-2.amazonaws.com/techops-interview-webapp/webapp.tar.gz ./

RUN tar xf webapp.tar.gz \ 
    && chmod +x ./dist/*

USER webapp

EXPOSE 3000/tcp 

CMD ["./dist/example-webapp-linux"]