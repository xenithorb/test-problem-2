# Interview Problem #2

<!-- @import "[TOC]" {cmd="toc" depthFrom=2 depthTo=5 orderedList=false} -->

<!-- code_chunk_output -->

- [Interview Problem #2](#interview-problem-2)
  - [Prerequisites](#prerequisites)
  - [Setup](#setup)
    - [AWS IAM](#aws-iam)
    - [Local AWS Credentials](#local-aws-credentials)
      - [Example Default AWS Provider settings](#example-default-aws-provider-settings)
    - [Installing Terraform](#installing-terraform)
    - [Installing Ansible](#installing-ansible)
  - [Usage and Deployment](#usage-and-deployment)
    - [Docker and docker-compose as a test (optional)](#docker-and-docker-compose-as-a-test-optional)
  - [Design Choices](#design-choices)
    - [Multi-node deployment](#multi-node-deployment)
      - [Terraform](#terraform)
      - [Ansible](#ansible)
      - [Diagram](#diagram)
    - [Scalable deployments](#scalable-deployments)
    - [Alternatives](#alternatives)
        - [k8s](#k8s)
        - [docker-compose](#docker-compose)
        - [Docker Swarm](#docker-swarm)
  - [Configuration](#configuration)
  - [Testing and Monitoring/Reporting/Alerting](#testing-and-monitoringreportingalerting)
    - [CI Testing](#ci-testing)
    - [Service Testing](#service-testing)
  - [External resources](#external-resources)

<!-- /code_chunk_output -->

## Prerequisites

* **Linux or other \*nix** based control machine with python `>=2.7` or `>=3.5` support (See: Ansible section)
* AWS IAM Account with API access via **access key** and **secret access key**
* The above credentials located in `~/.aws/credentials` under the `[default]` profile
* `terraform` installed on the control machine
* `ansible` installed on the control machine
* `git` installed on the local machine 
   > **Note**: Knowledge of `git` is assumed, See: [Getting Started - Installing git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git) for more information

## Setup

### AWS IAM

* Please refer to the AWS Documentation on [Creating an IAM User in your AWS Account](https://docs.aws.amazon.com/IAM/latest/UserGuide/id_users_create.html#id_users_create_console) - Be sure to retain the resulting **access key** and **secret access key** for the following step

### Local AWS Credentials

> I chose to use the standard `~/.aws/credentials` INI file as a place to store my AWS API credentials. This served two major benefits: I already had that in place with access to my default account for other projects, and it removed the need to store secrets in terraform's config, or in the environment.

> For more information please refer to the Terraform documentation on configuring the [AWS Provider](https://www.terraform.io/docs/providers/aws/)

* Please refer to the AWS Documentation on [Configuration and Credential Files](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-files.html) - This is a common file used to store AWS credentials for the popular `boto` library that both `awscli` and `terraform` (and `ansible`) take advantage of.

* Alternatively (but would require edits to this terraform config) you could elect to store temporary credentials in your environment by exporting `AWS_ACCESS_KEY_ID`, `AWS_SECRET_ACCESS_KEY` before running terraform.

#### Example Default AWS Provider settings

* Terraform `main.tf` (this project):

        provider "aws" {
            version                 = "~> 1.54"
            region                  = "us-west-1"
            shared_credentials_file = "~/.aws/credentials"
            profile                 = "default"
        }

* `~/aws/credentials` (on your local machine):

        [default]
        aws_access_key_id = <AWS_ACCESS_KEY_ID>
        aws_secret_access_key = <AWS_SECRET_ACCESS_KEY>

    Where `AWS_ACCESS_KEY_ID` and `AWS_SECRET_ACCESS_KEY` are replaced by your respective credentials.

### Installing Terraform

> Hashicorp tends to have an unusual way of distributing their software. Essentially, they only distribute monolithic binaries and expect them to be available in your `$PATH`. There are no packages or methods for traditional installation (including windows)

* Please refer to the Terraform [Downloads Page](https://www.terraform.io/downloads.html)
  
  > Note: Pay special attention to the information regarding checksums for production installs to ensure that your software is authentic

* Example Linux Install:

  * Check your `PATH` for your desired install location

        $ echo $PATH
        /usr/local/bin:/usr/bin:/bin:/usr/local/sbin:/usr/sbin:/home/username/.local/bin:/home/username/bin:/home/username/.local/bin:/home/username/bin
    > Many distributions these days are by default allowing the use of `~/.local/bin` and `~/bin` as valid locations for user-based binary placement in `PATH` - If you prefer a system based placement, use `/usr/local/bin` which requires `root` permissions to write to

  * Download and unpack the archive
  
        $ mkdir -p ~/.local/bin && cd ~/.local/bin
        $ tempfile=$(mktemp)
        $ wget https://releases.hashicorp.com/terraform/0.11.11/terraform_0.11.11_linux_amd64.zip -O $tempfile
        $ unzip -d . $tempfile
        $ rm -f $tempfile

  * Check and verify

        $ terraform -v
        Terraform v0.11.11

### Installing Ansible

> Ansible has various methods for installation. If you trust your distribution of Linux to maintain their package like I do on Fedora, I prefer using the package manager for installation. If not, ansible can be installed globally via `pip`

>Currently Ansible can be run from any machine with Python 2 (version 2.7) or Python 3 (versions 3.5 and higher) installed. Windows isn’t supported for the control machine See: [Control Machine Requirements](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html#control-machine-requirements)

* Example Linux package install

  * Fedora

        $ sudo dnf install -y ansible
  
  * CentOS / RHEL

        $ sudo yum install -y epel-release
        $ sudo yum install -y ansible

  * Universal `pip` installation. See: [Latest Releases via Pip](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html#latest-releases-via-pip)

        $ sudo easy_install pip
        $ sudo pip3 install ansible

  * Check and verify

        $ ansible --version
        ansible 2.7.5
          config file = /etc/ansible/ansible.cfg
          configured module search path = ['/home/<user>/.ansible/plugins/modules', '/usr/share/ansible/plugins/modules']
          ansible python module location = /usr/lib/python3.7/site-packages/ansible
          executable location = /usr/bin/ansible
          python version = 3.7.2 (default, Jan  3 2019, 09:14:01) [GCC 8.2.1 20181215 (Red Hat 8.2.1-6)]

## Usage and Deployment

* Clone the public repository

        $ git clone https://gitlab.com/xenithorb/test-problem-2.git
        $ cd test-problem-2

* Install `terraform` providers

        $ terraform init

* Generate a SSH key to be used as an AWS key pair
  
        $ ssh-keygen -t rsa -f prob2-keypair -N ""

    > Note: The name and path of the key `./prob2-keypair` and `./prob2-keypair.pub` is presently hard-coded in `main.tf`, which of course can be edited to use other credentials.

    > Note: `ssh-agent` with an already existing AWS key-pair may be used, but requires editing `main.tf` to support that by removing `--private-key '${local.ssh_key_name}'` from both `null_resource` resources, and by commenting out `resource "aws_key_pair" "prob2-keypair"`'s block. Also, inside both `resource "aws_instance` blocks, `key_name` also needs to match your custom key. Thus, the instructions to create a new key-pair for this assignment is the most straightforward approach for testing but not necessarily practical for a team or production

* View the planned changes to your AWS account

        $ terraform plan

    > Note: This assumes a local file state `terraform.tfstate` will be used, in production oriented environment [Remote State](https://www.terraform.io/docs/state/remote.html) is desired. 

* Apply the planned changes to your AWS account and **deploy the application**

        $ terraform apply

    This will create the required resources to deploy the project, and then use `ansible` to deploy the webapp from the provided S3 bucket (configurable in `./roles/prob2-webapp/defaults/main.yml` [here](https://gitlab.com/xenithorb/test-problem-2/blob/master/roles/prob2-webapp/defaults/main.yml))

    For your convenience, I've added some output information to the end of the terraform run that should look as follows:

        Apply complete! Resources: 8 added, 0 changed, 0 destroyed.

        Outputs:

        webapp_addresses =
        ----------------------------------------------------------------------------------------------
        Webapp Endpoint: http://ec2-13-56-150-27.us-west-1.compute.amazonaws.com:3000
        ----------------------------------------------------------------------------------------------
        prob2-redis   Public IP:  18.144.19.132
        prob2-redis  Private IP:  172.31.19.136
        prob2-webapp  Public IP:  13.56.150.27
        prob2-webapp Private IP:  172.31.27.191
        ----------------------------------------------------------------------------------------------
        Public SSH key: ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDSrJDHsfR4sWY9Z/KXrPQdGAwadGmse2WrCsLCjghwqiw429+RWQQLYPmmtV+VtNwcJHFgV1p4RU8RQgryV1I4FGWKa9HeDr5xBp1zgFzdUTBl6XahspELNkZC5/OGeRZIpjJXqelSj65vUIuIdM9nOe7OqoZJl5XoWJQl0/AeWjbpx1MJYxjIOVhVfILR4ijkvdYEtf0tTBkg0JEMQoLSIOdrf0QZMRO3sIYLxTvSA/8Q1wmhCVIe/R9uXGbcFb7Zc05g2aF7bHlhfoIocoie9u0BzkjTmsY4In5eMC+AT+CAfnPVvn/iFlOE9Z0vTv2jDg84FITUrTmBh6ZlBOTV

    > Note: Temporarily try reaching http://ec2-13-56-150-27.us-west-1.compute.amazonaws.com:3000 !

    > Please see the log of the **full run** here: https://gitlab.com/xenithorb/test-problem-2/blob/master/APPENDIX1.log
* Re-deployment of the application

        $ terraform apply -auto-approve

    >Note: The terraform configuration has been made in such a way as to conform to the **"developers feel strongly about running a single command"** criterion by running the ansible provisioners every time. This has the effect of re-deploying the application directly from the binary source without regard for testing anything prior to it being built. In production it would be best to automate this sort of thing from a pipeline that also tests the source code (which for this exercise I haven't been given access to)

### Docker and docker-compose as a test (optional)

> I also wanted to showcase the app by using `docker` and `docker-compose`, since it's hard to talk about containers unless you show that you know how to use them.

> Note: This requires `docker-compose` and `docker` to be installed on your **local machine**.  On Fedora, simply `sudo dnf install -y docker-compose`, for other platforms, please refer to the reference documentation: https://docs.docker.com/compose/install/

  * Start the stack with `docker-compose`
  
        $ docker-compose up -d
        Creating network "test-problem-2_backend" with the default driver
        Creating network "test-problem-2_frontend" with the default driver
        Creating test-problem-2_webapp_1 ... done
        Creating test-problem-2_redis_1  ... done
        $ docker-compose ps
                Name                        Command               State           Ports
        -----------------------------------------------------------------------------------------
        test-problem-2_redis_1    docker-entrypoint.sh redis ...   Up      6379/tcp
        test-problem-2_webapp_1   ./dist/example-webapp-linux      Up      0.0.0.0:3000->3000/tcp

    > Then simply navigate to: http://localhost:3000

## Design Choices

### Multi-node deployment

The exercise itself does not call for a multi-node deployment, but deploying monolithic servers, especially when you're leveraging a powerful tool like `terraform` just seems _wrong._ In a micro-services architecture, resources are generally broken up into _tiers_, and those tiers are often delineated by being on different hosts or containers. A set of proxies per tier and some shared endpoints and you have yourself a fairly easy traditional way of horizontally scaling.

For now and for this exercise, the deployment strategy (as requested) is for a developer to run a single command - `terraform apply -auto-approve` that will ensure the state of the infrastructure and re-deploy the S3 binary.

Even though I [eventually found the original source from jaxxstorm](https://github.com/jaxxstorm/example-webapp-go), I wish I found it sooner and I maybe would have done a build pipeline. Build, Test, and deployment pipelines are were I come from presently, and practice that I value as being integral in modern development and deployment. (i.e. CI/CD in general)


   >Caveat: My choices were largely influenced by wanting to showcase my abilities with `terraform` and `ansible`, knowing that `terraform` is a tool like I will likely be working with as a member of the team. I chose `ansible` because I'm intimately familiar with it as my go-to for configuration management. Everything in this exercise could also have been accomplished with ansible, but that would not have afforded the opportunity to use multiple tools.

#### Terraform

* Main orchestration tool that does exceedingly well with managing cloud providers
* Maintains a state of the infrastructure to manage configuration _drift_ (whereas ansible relies solely on idempotency and as a result tends to be slower)
* Not typically used for configuration internal to systems, though provisioners exist that allow a granular level of scripting control regardless 

#### Ansible

* An agent-less config-management tool that can be used to configure a host remotely (typically over SSH), or locally (by running a local playbook or the `ansible-pull` method)
* Relies on idempotency to control drift, with a necessity to run the entire playbook over again to check state
* Very effective at replacing bash scripting with a clean and organized role and playbook based structure that uses YAML
* Quick at prototyping configurations with a very low level of opinionated direction 

#### Diagram


```mermaid
graph LR
  subgraph Frontend
    webapp(webapp :3000)
  end

  subgraph Data
    redis(redis  :6379)
  end
  browser --> webapp
  webapp --> redis
```

### Scalable deployments

**"A production-grade solution is not something we expect"**

At the risk of spending too much time on the exercise, I decided not to go full-on scalable. The quote from the problem sheet communicated to me that, while you should complete the task thorougly and aptly, don't go totally nuts. To me, deploying in a scalable manner is the delineation between "testing and dev" and "production grade."

 Though the application itself is perfectly capable of scaling out because it's stateless (which is ought to be given that the source was orginally a kubernetes example - [guestbook-go](https://github.com/kubernetes/examples/tree/master/guestbook-go)) 

The application itself is stateless because it uses a separate tier to save its information - namely `redis`, and as such you can scale unbounded so long as your access to the data tier is available by it also likely being a clustered and proxied endpoint.

In order to promote load balancing and enable High Availability in front of the application, you're also going to want to involve your favorite flavor of proxy, whether it be NGINX, HAproxy, something vendor specific like ELB, or even a containerized orchestrated approach like `kube-proxy` with Kubernetes

For example, contrast the above diagram with one that I did with ansible a year ago:

```mermaid
graph LR
  subgraph Internal
    cr1((Call Routing))
    tq1((Task Queue))
    tce1((Telecom Events))
  end

  subgraph Internet
    ab1{Agent Browser}
  end

  subgraph haproxy-nginx
    ha-ng-1["haproxy-nginx-1 <br/> keepalived (MASTER)"]
    ha-ng-2["haproxy-nginx-2 <br/> keepalived (BACKUP)"]
  end

  subgraph haproxy-node
    ha-no-1["haproxy-node-1 <br/> keepalived (MASTER)"]
    ha-no-2["haproxy-node-2 <br/> keepalived (BACKUP)"]
  end

  subgraph redis-sentinel-cluster
    redis1[redis-1 <br/> redis-sentinel-1 <br/> MASTER]
    redis2[redis-2 <br/> redis-sentinel-2 <br/> SLAVE]
    redis3[redis-3 <br/> redis-sentinel-3 <br/> SLAVE]
  end

  cr1 --> ha-ng-1
  tq1 --> ha-ng-1
  tce1 --> ha-ng-1

  ha-ng-1 --- nginx-phpfpm-2
  ha-ng-1 --- nginx-phpfpm-1
  ha-ng-2 --- nginx-phpfpm-2
  ha-ng-2 --- nginx-phpfpm-1

  nginx-phpfpm-1 --> redis1
  nginx-phpfpm-2 --> redis1
  redis1 --> node-1
  redis1 --> node-2

  node-1 --> ha-no-1
  node-1 --> ha-no-2
  node-2 --> ha-no-1
  node-2 --> ha-no-2

  ha-no-1 --- ab1
```
>This is a call-center's notification stack that sends events back to a browser via `nodejs` and `socket.io`

If asked, my wish for this implementation would have been service discovery, like `consul`, but the project and time constraints, and treatment of the servers like pets rather than cattle, didn't really warrant that level of effort.

### Alternatives

##### k8s

Of course, this application was _designed_ as a "Hello World" flavored example for kubernetes deployments, so that is unsurprisingly going to be a fine way of scaling it out, too. This is of course the desired method to move forward with contemporarily, and is what I would have selected were I wanting to take this application into production. The reason that I chose not to use it here in this test was simply due to time constraints, and I felt that since I already communicated that I've studied up on k8s but haven't used it in production yet, it might not have been a good test or effective use of time managing a simple-yet-not-production-grade deployment example.

##### docker-compose 

Another idea I considered while writing the [`Dockerfile`](https://gitlab.com/xenithorb/test-problem-2/blob/master/Dockerfile) and [`docker-compose.yml`](https://gitlab.com/xenithorb/test-problem-2/blob/master/docker-compose.yml), was to use that to deploy based on re-building the container (which would re-grab the binary archive - although in its present state I realize the Dockerfile won't notice changes from the S3 binary, so it will need to be built from scratch each time). While I think docker-compose is a fine solution to do quick development and prototyping, I've always advocated not using it as a method for deployment, especially in production. I believe you shouldn't bring containers into production unless you're ready to properly _orchestrate_ them, and that's where docker-compose always falls short. (as it was not intended for that)

##### Docker Swarm

An honorable mention that I've toyed with in the past, but not one that I've seen taken very seriously by the DevOps community. Docker swarm meets the bare minimum for orchestration requirements, but doesn't have the buy-in from the community or employers, and thus isn't worth taking seriously. 

## Configuration

My design choice necessitated configuration right from the start, since I absolutely needed the service to have a configured redis endpoint.

Unfortunately I didn't have any guidance on the configuration options available, nor did I have a link to the source code (that I later found anyway). So, I simply used `strace` to see what files it was looking for and went with the `config.toml` format that I ended up templating in ansible (which since the application accepted that, was just easier to template than YAML):

```toml
redishost = "{{ prob2_redis_host }}"
redisport = {{ prob2_redis_port }}
```
One important point worth highlighting is that in the docker-compose implementation, I'm using the environment variables which is exactly the apt approach for a stateless application (I too am a [12-factor](https://12factor.net/config) application advocate)

```yaml
    environment:
      REDISHOST: redis
      REDISPORT: 6379
```
This method of on-the-fly configuration is absolutely the preferred method when containerizing applications because of the nature of ephemeral containers, and would be used frequently with kubernetes deployments where containers aren't guaranteed to perpetuate, or be pre-configured on instantiation.

> Note: I could have technically used the environment variables in the systemd service file, but 1) wasn't aware that it took environment variables at that time, and 2) I find for VMs a cleaner approach is a config file written and managed by an CM tool because admins don't typically think of systemd units as the place to look for running configurations.

## Testing and Monitoring/Reporting/Alerting

### CI Testing

Some CI testing has been implemented with _Gitlab-CI_, here are some interesting URLs:

Type    | Stage | Jobname |  URL
--------|------------|----|----
Config | N/A | N/A | https://gitlab.com/xenithorb/test-problem-2/blob/master/.gitlab-ci.yml
Job | Test | Test:Ansible | https://gitlab.com/xenithorb/test-problem-2/-/jobs/143771348
Job | Test | Test:Terraform | https://gitlab.com/xenithorb/test-problem-2/-/jobs/143771347
Job | Deploy | Deploy:Terraform | https://gitlab.com/xenithorb/test-problem-2/-/jobs/143771349
Pipeline | Visual | N/A | https://gitlab.com/xenithorb/test-problem-2/pipelines/42792923

Currently it:

- Syntax Checks and Lints the ansible playbooks
- Initializes and Validates the terraform configuration

> Note: `Deploy:Terraform` is a "mock" manual job that describes the direction I would have taken if I wanted to deploy from the pipeline. It was my impression that deploying from the pipeline is well outside of the scope that problem #2 requires.

### Service Testing

Currently I am doing a minimal amount of **service** testing by passing the necessary information from `terraform` to `ansible` in the form of commandline arguments. This allows ansible to "know" information that it can't gather or know on its own, information that typically would be in a static `inventory` file or a dynamic inventory (which is still technically possible because of using AWS here)

The test are as follows, for the `redis` plabook:

```yaml
    - name: Check for redis availability
      wait_for:
        host: "{{ ansible_default_ipv4.address }}"
        port: 6379
        state: present
        timeout: 10
      delegate_to: "{{ webapp_host }}"
```
> This checks _from_ the webapp host (due to the security group setup, the webapp host is the only one able to reach 6379 on the redis host) to see if the port is open.

For the `webapp` playbook:

```yaml
   - name: Check for basic HTTP connectivity
     uri:
       url: "{{ http_endpoint }}"
       return_content: yes
     register: http_check

   - name: Fail if HTTP check fails
     fail:
       msg: "HTTP Endpoint not accessible"
     when: "'DOCTYPE' not in http_check.content"
```
> A very minimal example of testing for basic HTTP connectivity from the control machine.

No monitoring or alerting has been setup, but for such a small project as this, I may have used an equally small monitoring and alerting tool called `monit`: <https://mmonit.com/monit/> which does a fantastic job at watchdogging such things. That would however require another resource like a VM. On the other hand, Kubernetes has features that allow you to do orchestration tasks based on liveliness and readiness probes, which is the appropriate respective feature set to monitor a container with this application.

You could also use platform specific tooling like CloudWatch to monitor host resource usage, and Route 53 to do health checking, but that's strictly related to virtual machine deployments.

As far as metrics/charts, I'm not exactly sure how to approach that. I might just be lacking imagination in that regard. Perhaps you could use a tool to pull data from redis and stuff it into some logging stack like ELK, or prometheus/grafana, and visualize how many requests are being made. You could do the same for system resources too, but this can always be a deep rabbit hole.

In the past I've used Zabbix, nagios, and cacti to do various monitoring, visualizing, and alerting, but they're kinda old-school tooling and I'm aware that I'm not up-to speed on everything involved in doing that for an orchestration platform.

## External resources

Aside from my own knowledge, and the already linked documentation I've scattered throughout this document, I used the following pages:

1. https://www.terraform.io/docs/providers/aws/index.html
2. https://www.terraform.io/docs/providers/aws/r/instance.html
3. https://docs.docker.com/reference/ (I did in fact make a Dockerfile and docker-compose.yml to initially test)
4. https://alex.dzyoba.com/blog/terraform-ansible/
5. https://hackernoon.com/using-terraform-for-gitlab-review-apps-acf05920a264
6. https://docs.ansible.com/ansible/latest/modules (various modules)
7. `man systemd.unit`
8. `man systemd.service`