variable "ec2_defaults" {
  default {
    instance_type = "t3.micro"
    user          = "centos"
  }
}

variable "ports" {
  default {
    redis.port   = 6379
    redis.proto  = "tcp"
    webapp.port  = 3000
    webapp.proto = "tcp"
  }
}

locals {
  ssh_key_name     = "prob2-keypair"
  ssh_pub_key_data = "${file("${local.ssh_key_name}.pub")}"
  ssh_sec_key_data = "${file("${local.ssh_key_name}")}"
  http_endpoint    = "http://${aws_instance.prob2-webapp.public_dns}:${var.ports["webapp.port"]}"
}

provider "aws" {
  version                 = "~> 1.54"
  region                  = "us-west-1"
  shared_credentials_file = "~/.aws/credentials"
  profile                 = "default"
}

provider "null" {
  version = "~> 1.0"
}

data "aws_ami" "centos" {
  most_recent = true

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  filter {
    name   = "owner-alias"
    values = ["aws-marketplace"]
  }

  filter {
    name   = "product-code"
    values = ["aw0evgkw8e5c1q413zgy5pjce"] # Official CentOS.org
  }
}

resource "aws_key_pair" "prob2-keypair" {
  key_name   = "${local.ssh_key_name}"
  public_key = "${local.ssh_pub_key_data}"
}

resource "aws_security_group" "prob2-webapp" {
  name        = "prob2-webapp"
  description = "Problem 2 Webapp"

  ingress {
    from_port   = "${var.ports["webapp.port"]}"
    to_port     = "${var.ports["webapp.port"]}"
    protocol    = "${var.ports["webapp.proto"]}"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "prob2-redis" {
  name        = "prob2-redis"
  description = "Problem 2 Redis"

  ingress {
    from_port       = "${var.ports["redis.port"]}"
    to_port         = "${var.ports["redis.port"]}"
    protocol        = "${var.ports["redis.proto"]}"
    security_groups = ["${aws_security_group.prob2-webapp.id}"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "prob2-ssh" {
  name        = "prob2-ssh"
  description = "Problem 2 SSH"

  ingress {
    from_port   = "22"
    to_port     = "22"
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_instance" "prob2-webapp" {
  ami                    = "${data.aws_ami.centos.id}"
  instance_type          = "${var.ec2_defaults["instance_type"]}"
  vpc_security_group_ids = ["${aws_security_group.prob2-webapp.id}", "${aws_security_group.prob2-ssh.id}"]
  key_name               = "${aws_key_pair.prob2-keypair.key_name}"

  tags = {
    Name = "prob2-webapp"
    Role = "Problem 2 Solution"
  }

  provisioner "remote-exec" {
    inline = ["sudo yum install -y python"]

    connection {
      type        = "ssh"
      user        = "${var.ec2_defaults["user"]}"
      private_key = "${local.ssh_sec_key_data}"
    }
  }
}

resource "aws_instance" "prob2-redis" {
  ami                    = "${data.aws_ami.centos.id}"
  instance_type          = "${var.ec2_defaults["instance_type"]}"
  vpc_security_group_ids = ["${aws_security_group.prob2-redis.id}", "${aws_security_group.prob2-ssh.id}"]
  key_name               = "${aws_key_pair.prob2-keypair.key_name}"

  tags = {
    Name = "prob2-redis"
    Role = "Problem 2 Solution"
  }

  # This is mainly to ensure we have what ansible needs, but also helps with ensuring
  # SSH is ready and available for the following null_resource provisions
  provisioner "remote-exec" {
    inline = ["sudo yum install -y python"]

    connection {
      type        = "ssh"
      user        = "${var.ec2_defaults["user"]}"
      private_key = "${local.ssh_sec_key_data}"
    }
  }
}

# Null_resource is used in lieu of a "normal" provison run as somewhat of a "hack"
# To fit the criteria that it will always run and re-deploy updated S3 artifacts
resource "null_resource" "webapp-rerun" {
  triggers {
    rerun = "${uuid()}"
  }

  provisioner "local-exec" {
    command = "ansible-playbook -u ${var.ec2_defaults["user"]} -i '${aws_instance.prob2-webapp.public_ip},' --private-key '${local.ssh_key_name}' provision-webapp.yml -e redis_host=${aws_instance.prob2-redis.private_ip} -e redis_port=6379 -e http_endpoint=${local.http_endpoint}"

    environment {
      ANSIBLE_HOST_KEY_CHECKING = "False"
    }
  }
}

resource "null_resource" "redis-rerun" {
  triggers {
    rerun = "${uuid()}"
  }

  provisioner "local-exec" {
    command = "ansible-playbook -u ${var.ec2_defaults["user"]} -i '${aws_instance.prob2-redis.public_ip},' --private-key '${local.ssh_key_name}' provision-redis.yml -e webapp_host=${aws_instance.prob2-webapp.public_ip}"

    environment {
      ANSIBLE_HOST_KEY_CHECKING = "False"
    }
  }
}

# outputs useful endpoint information for viewing the web portal / SSH endpoints
output "webapp_addresses" {
  value = <<EOF
  
----------------------------------------------------------------------------------------------
Webapp Endpoint: ${local.http_endpoint}
----------------------------------------------------------------------------------------------
${aws_instance.prob2-redis.tags["Name"]}   Public IP:  ${aws_instance.prob2-redis.public_ip}
${aws_instance.prob2-redis.tags["Name"]}  Private IP:  ${aws_instance.prob2-redis.private_ip}
${aws_instance.prob2-webapp.tags["Name"]}  Public IP:  ${aws_instance.prob2-webapp.public_ip}
${aws_instance.prob2-webapp.tags["Name"]} Private IP:  ${aws_instance.prob2-webapp.private_ip}
----------------------------------------------------------------------------------------------
Public SSH key: ${local.ssh_pub_key_data}
EOF
}
